#![no_main]
#![no_std]

use panic_semihosting as _;
use stm32h7xx_hal as _;
use cortex_m_semihosting::hprintln;

use tcp_client::hardware::*;
use tcp_client::network::{NetworkWorker, TcpClient};
use fugit::ExtU64;
use smoltcp_nal::embedded_nal::{IpAddr, Ipv4Addr};
use stm32h7xx_hal::gpio;

pub struct NetworkObjects {
  network_worker: NetworkWorker,
  tcp_client: TcpClient
}

#[rtic::app(device = stm32h7xx_hal::pac, peripherals = true, dispatchers = [UART8])]
mod app {
  use tcp_client::hardware;

  use super::*;
  
  #[monotonic(binds = SysTick, default = true, priority = 2)]
  type Monotonic = Systick;

  #[shared]
  struct Shared {
    network_objects: NetworkObjects
  }

  #[local]
  struct Local {
    diode: gpio::gpiod::PD3<gpio::Output<gpio::PushPull>>
  }

  #[init (local = [nm: Option<NetworkManager> = None])]
  fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
    let clock = SystemTimer::new(|| monotonics::now().ticks() as u32);

    let board = hardware::setup::setup(cx.core, cx.device, clock);
    *cx.local.nm = Some(NetworkManager::new(board.network_device.stack));
    // task_blink::spawn_after(1.secs().into()).unwrap();

    let network_objects = NetworkObjects {
      network_worker: NetworkWorker::new(cx.local.nm.as_ref().unwrap().acquire_stack()),
      tcp_client: TcpClient::new(cx.local.nm.as_ref().unwrap().acquire_stack())};

    (
      Shared {
        network_objects
      },
      Local {
        diode: board.diode
      },
      init::Monotonics(board.systick)
    )
  }

  #[idle(shared = [network_objects])]
  fn idle(mut cx: idle::Context) -> ! {
    hprintln!("idle");

    task_connect::spawn().unwrap();
    loop {
      cx.shared.network_objects.lock(|no| no.network_worker.work());
    }
  }

  #[task(shared = [network_objects], local = [diode])]
  fn task_read(mut cx: task_read::Context) {
    if !cx.shared.network_objects.lock(|no| no.tcp_client.is_connected()) {
      task_connect::spawn().unwrap();
      return;
    }

    let mut buffer = [0u8; 10];
    cx.shared.network_objects.lock(|no| {
      match no.tcp_client.receive(|data| {
        buffer.clone_from_slice(&data[0..10])
      }) {
        Ok(_) => {
          // hprintln!("Received: {:?}", buffer);
          if buffer[0] == 49 {
            cx.local.diode.set_high();
          } else if buffer[0] == 50 {
            cx.local.diode.set_low();
          }
          match no.tcp_client.send(&buffer) {
            Ok(_) => (),
            Err(_) => {hprintln!("Failed to send data!")}
          }},
        Err(_) => {hprintln!("Failed to read data!");}
      }
    });

    task_read::spawn_after(1.secs()).unwrap();
  }

  #[task(shared = [network_objects], local = [])]
  fn task_connect(mut cx: task_connect::Context) {
    match cx.shared.network_objects.lock(|no|
      no.tcp_client.connect(IpAddr::V4(Ipv4Addr::new(192, 168, 1, 1)), 1234)
    ) {
      Ok(_) => {
        hprintln!("Connected to target");
        task_read::spawn().unwrap();
      },
      Err(_) => {
        hprintln!("Failed to connect to target!");
        task_connect::spawn_after(1.secs()).unwrap();
      },
    }
  }
}
