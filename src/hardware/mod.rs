pub mod setup;

use stm32h7xx_hal::time::Hertz;
use stm32h7xx_hal::ethernet;

/// System timer (RTIC Monotonic) tick frequency
pub const MONOTONIC_FREQUENCY: u32 = 1_000;
pub type Systick = systick_monotonic::Systick<MONOTONIC_FREQUENCY>;
pub type SystemTimer = mono_clock::MonoClock<u32, MONOTONIC_FREQUENCY>;

pub const SYSCLK: Hertz = Hertz::MHz(400);
pub const TIMER_FREQUENCY: Hertz = Hertz::MHz(100);

const TX_DESRING_CNT: usize = 4;
const RX_DESRING_CNT: usize = 4;

pub type NetworkStack = smoltcp_nal::NetworkStack<
    'static,
    ethernet::EthernetDMA<'static, TX_DESRING_CNT, RX_DESRING_CNT>,
    SystemTimer,
>;

pub type NetworkManager = smoltcp_nal::shared::NetworkManager<
    'static,
    ethernet::EthernetDMA<'static, TX_DESRING_CNT, RX_DESRING_CNT>,
    SystemTimer,
>;

pub type EthernetPhy = ethernet::phy::LAN8742A<ethernet::EthernetMAC>;
