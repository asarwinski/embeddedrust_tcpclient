use super::*;

use stm32h7xx_hal::pwr::PwrExt;
use stm32h7xx_hal::stm32::{CorePeripherals, Peripherals};
use stm32h7xx_hal::ethernet::{self, PHY};
use stm32h7xx_hal::prelude::*;
use smoltcp_nal::smoltcp;
use stm32h7xx_hal::gpio;

pub struct NetworkDevice {
  pub stack: NetworkStack,
  pub phy: EthernetPhy,
  pub mac_address: smoltcp::wire::EthernetAddress,
}

pub struct BoardDevices {
  pub systick: Systick,
  pub network_device: NetworkDevice,
  pub diode: gpio::gpiod::PD3<gpio::Output<gpio::PushPull>>
}

#[link_section = ".sram3.eth"]
static mut DES_RING: ethernet::DesRing<
    { TX_DESRING_CNT },
    { RX_DESRING_CNT },
> = ethernet::DesRing::new();

const NUM_TCP_SOCKETS: usize = 2;
const NUM_UDP_SOCKETS: usize = 0;
const NUM_SOCKETS: usize = NUM_UDP_SOCKETS + NUM_TCP_SOCKETS;

pub struct NetStorage {
  pub ip_addrs: [smoltcp::wire::IpCidr; 1],

  // Note: There is an additional socket set item required for the DHCP socket.
  pub sockets: [smoltcp::iface::SocketStorage<'static>; NUM_SOCKETS + 1],
  pub tcp_socket_storage: [TcpSocketStorage; NUM_TCP_SOCKETS],
  pub udp_socket_storage: [UdpSocketStorage; NUM_UDP_SOCKETS],
  pub neighbor_cache:
      [Option<(smoltcp::wire::IpAddress, smoltcp::iface::Neighbor)>; 8],
  pub routes_cache:
      [Option<(smoltcp::wire::IpCidr, smoltcp::iface::Route)>; 8],
}

#[derive(Copy, Clone)]
pub struct UdpSocketStorage {
    rx_storage: [u8; 1024],
    tx_storage: [u8; 2048],
    tx_metadata:
        [smoltcp::storage::PacketMetadata<smoltcp::wire::IpEndpoint>; 10],
    rx_metadata:
        [smoltcp::storage::PacketMetadata<smoltcp::wire::IpEndpoint>; 10],
}

impl UdpSocketStorage {
    const fn new() -> Self {
        Self {
            rx_storage: [0; 1024],
            tx_storage: [0; 2048],
            tx_metadata: [smoltcp::storage::PacketMetadata::<
                smoltcp::wire::IpEndpoint,
            >::EMPTY; 10],
            rx_metadata: [smoltcp::storage::PacketMetadata::<
                smoltcp::wire::IpEndpoint,
            >::EMPTY; 10],
        }
    }
}

#[derive(Copy, Clone)]
pub struct TcpSocketStorage {
    rx_storage: [u8; 1024],
    tx_storage: [u8; 1024],
}

impl TcpSocketStorage {
    const fn new() -> Self {
        Self {
            rx_storage: [0; 1024],
            tx_storage: [0; 1024],
        }
    }
}

impl Default for NetStorage {
  fn default() -> Self {
      NetStorage {
          // Placeholder for the real IP address, which is initialized at runtime.
          ip_addrs: [smoltcp::wire::IpCidr::Ipv6(
              smoltcp::wire::Ipv6Cidr::SOLICITED_NODE_PREFIX,
          )],
          neighbor_cache: [None; 8],
          routes_cache: [None; 8],
          sockets: [smoltcp::iface::SocketStorage::EMPTY; NUM_SOCKETS + 1],
          tcp_socket_storage: [TcpSocketStorage::new(); NUM_TCP_SOCKETS],
          udp_socket_storage: [UdpSocketStorage::new(); NUM_UDP_SOCKETS],
      }
  }
}

pub fn setup(
  core: CorePeripherals,
  device: Peripherals,
  clock: SystemTimer,
) -> BoardDevices {
  device.RCC.ahb2enr.modify(|_, w| w.sram3en().set_bit());
  device.RCC.rsr.write(|w| w.rmvf().set_bit());

  let pwr = device.PWR.constrain();
  let vos = pwr.freeze();

  let rcc = device.RCC.constrain();
  let ccdr = rcc
    .use_hse(8.MHz())
    .sysclk(SYSCLK)
    .hclk(200.MHz())
    .per_ck(TIMER_FREQUENCY)
    .pll2_p_ck(100.MHz())
    .pll2_q_ck(100.MHz())
    .freeze(vos, &device.SYSCFG);

  let systick = Systick::new(core.SYST, ccdr.clocks.sysclk().to_Hz());

  let mut delay = asm_delay::AsmDelay::new(asm_delay::bitrate::Hertz(
    ccdr.clocks.c_ck().to_Hz(),
  ));

  let gpioa = device.GPIOA.split(ccdr.peripheral.GPIOA);
  let gpiob = device.GPIOB.split(ccdr.peripheral.GPIOB);
  let gpioc = device.GPIOC.split(ccdr.peripheral.GPIOC);
  let gpiod = device.GPIOD.split(ccdr.peripheral.GPIOD);
  let gpioe = device.GPIOE.split(ccdr.peripheral.GPIOE);
  // let gpiof = device.GPIOF.split(ccdr.peripheral.GPIOF);
  let gpiog = device.GPIOG.split(ccdr.peripheral.GPIOG);

  let mac_addr = smoltcp::wire::EthernetAddress([0x00, 0x0b, 0x00, 0x00, 0x00, 0x00]);

  let network_device = {
    let eithernet_pins = {
      let mut eth_phy_nrst = gpioe.pe3.into_push_pull_output();
      eth_phy_nrst.set_low();
      delay.delay_us(200u8);
      eth_phy_nrst.set_high();
      let rmii_ref_clk = gpioa
          .pa1
          .into_alternate();
      let rmii_mdio = gpioa
          .pa2
          .into_alternate();
      let rmii_mdc = gpioc
          .pc1
          .into_alternate();
      let rmii_crs_dv = gpioa
          .pa7
          .into_alternate();
      let rmii_rxd0 = gpioc
          .pc4
          .into_alternate();
      let rmii_rxd1 = gpioc
          .pc5
          .into_alternate();
      let rmii_tx_en = gpiog
          .pg11
          .into_alternate();
      let rmii_txd0 = gpiog
          .pg13
          .into_alternate();
      let rmii_txd1 = gpiob
          .pb13
          .into_alternate();
      (
          rmii_ref_clk,
          rmii_mdio,
          rmii_mdc,
          rmii_crs_dv,
          rmii_rxd0,
          rmii_rxd1,
          rmii_tx_en,
          rmii_txd0,
          rmii_txd1,
      )
    };

    let (eth_dma, eth_mac) = ethernet::new(
      device.ETHERNET_MAC,
      device.ETHERNET_MTL,
      device.ETHERNET_DMA,
      eithernet_pins,
      unsafe { &mut DES_RING },
      mac_addr,
      ccdr.peripheral.ETH1MAC,
      &ccdr.clocks
    );

    let mut lan8742a = ethernet::phy::LAN8742A::new(eth_mac.set_phy_addr(0));
    lan8742a.phy_reset();
    lan8742a.phy_init();

    unsafe { ethernet::enable_interrupt() };

    let ip_address = smoltcp::wire::IpAddress::v4(192, 168, 1, 19);
    let store = cortex_m::singleton!(: NetStorage = NetStorage::default()).unwrap();
    store.ip_addrs[0] = smoltcp::wire::IpCidr::new(ip_address, 24);
    
    let mut routes = smoltcp::iface::Routes::new(&mut store.routes_cache[..]);
    routes.add_default_ipv4_route(smoltcp::wire::Ipv4Address::UNSPECIFIED).unwrap();

    let neighbor_cache = smoltcp::iface::NeighborCache::new(&mut store.neighbor_cache[..]);
    let mut interface = smoltcp::iface::InterfaceBuilder::new(eth_dma, &mut store.sockets[..])
      .hardware_addr(smoltcp::wire::HardwareAddress::Ethernet(mac_addr))
      .neighbor_cache(neighbor_cache)
      .ip_addrs(&mut store.ip_addrs[..])
      .routes(routes)
      .finalize();
    
    for storage in store.tcp_socket_storage[..].iter_mut() {
      let tcp_socket = {
        let rx_buffer = smoltcp::socket::TcpSocketBuffer::new(&mut storage.rx_storage[..]);
        let tx_buffer = smoltcp::socket::TcpSocketBuffer::new(&mut storage.tx_storage[..]);

        smoltcp::socket::TcpSocket::new(rx_buffer, tx_buffer)
      };
      interface.add_socket(tcp_socket);
    }

    for storage in store.udp_socket_storage[..].iter_mut() {
      let udp_socket = {
        let rx_buffer = smoltcp::socket::UdpSocketBuffer::new(
          &mut storage.rx_metadata[..],
          &mut storage.rx_storage[..]
        );
        let tx_buffer = smoltcp::socket::UdpSocketBuffer::new(
          &mut storage.tx_metadata[..],
          &mut storage.tx_storage[..]
        );
        smoltcp::socket::UdpSocket::new(rx_buffer, tx_buffer)
      };
      interface.add_socket(udp_socket);
    }

    let stack = smoltcp_nal::NetworkStack::new(interface, clock);

    NetworkDevice {
      stack,
      phy: lan8742a,
      mac_address: mac_addr
    }
  };

  let mut diode = gpiod.pd3.into_push_pull_output();
  diode.set_low();
  BoardDevices { systick, network_device, diode }
}
