use smoltcp_nal::smoltcp::iface::SocketHandle;

use crate::hardware::*;

use core::option::Option;
use smoltcp_nal::embedded_nal::TcpClientStack;
use smoltcp_nal::embedded_nal::IpAddr;

pub type NetworkReference =
  smoltcp_nal::shared::NetworkStackProxy<'static, NetworkStack>;

pub struct NetworkWorker {
  stack: NetworkReference
}

impl NetworkWorker {
  pub fn new(stack: NetworkReference) -> Self {
    return Self {stack};
  }

  pub fn work(&mut self) {
    match self.stack.lock(|stack| stack.poll()) {
      Ok(true) => (),
      Ok(false) => (),
      Err(_) => ()
    }
  }
}

pub struct TcpClient {
  stack: NetworkReference,
  socket: Option<SocketHandle>
}

impl TcpClient {
  pub fn new(stack: NetworkReference) -> Self {
    TcpClient {stack, socket: Option::None}
  }

  pub fn connect(&mut self, ip: IpAddr, port: u16) -> Result<(),()> {
    let mut socket = match self.stack.lock(|stack| stack.socket()) {
      Ok(socket_handle) => socket_handle,
      Err(_) => {return Err(())}
    };

    let address = smoltcp_nal::embedded_nal::SocketAddr::new(ip, port);
    match self.stack.lock(|stack| stack.connect(&mut socket, address)) {
      Ok(_) => {
        self.socket = Some(socket);
        Ok(())
      },
      Err(_) => Err(())
    }
  }

  pub fn send(&mut self, data: &[u8]) -> Result<(), ()> {
    match self.socket.as_mut() {
      Some(mut socket) => {
        match self.stack.lock(|stack| stack.send(&mut socket, data)) {
          Ok(_) => Ok(()),
          Err(_) => Err(())
        }
      }
      None => Err(())
    }
  }

  pub fn receive<F>(&mut self, mut f: F) -> Result<(), ()>
  where
    F: FnMut(&[u8]) 
  {
    let mut buffer = [0u8; 255];
    match self.socket.as_mut() {
      Some(socket) => {
        match self.stack.lock(|stack| stack.receive(socket, &mut buffer)) {
          Ok(_) => {
            f(&buffer);
            Ok(())
          }
          Err(_) => Err(())
        }
      }
      None => Err(())
    }
  }

  pub fn is_connected(&mut self) -> bool {
    match self.socket {
      Some(socket) => self.stack.lock(|stack| stack.is_connected(&socket).is_ok()),
      None => false
    }
  }
}
